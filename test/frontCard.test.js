const supertest = require('supertest');
const app = require('../app');

describe("Probar el añadir una frontCard", ()=>{
    it("Debería de obtener un frontCard añadido", (done)=>{
        supertest(app).post("/frontCard")
        .expect(200)
        .end(function(err, res){
            if(err){
                done(err);
            }else{
                done();
            }
        });
    });
});
it("No debería de obtener una backcard añadido", (done)=>{
    supertest(app).post("/frontCard/1")
    .expect(403)
    .end(function(err, res){
        if(err){
            done(err);
        }else{
            done();
        }
    });
});

describe("Probar el obtener todas las frontCards", ()=>{
    it("Debería de obtener una lista de frontCards", (done)=>{
        supertest(app).get("/frontCard")
        .expect(200)
        .end(function(err, res){
            if(err){
                done(err);
            }else{
                done();
            }
        });
    });
});
it("No debería de obtener una lista de frontCards", (done)=>{
    supertest(app).get("/frontCard/2d")
    .expect(403)
    .end(function(err, res){
        if(err){
            done(err);
        }else{
            done();
        }
    });
});