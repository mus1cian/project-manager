const supertest = require('supertest');
const app = require('../app');

describe("Probar el abrir el programa", ()=>{
    it("Debería de obtener un título 'Express'", (done)=>{
        supertest(app).get("/index")
        .expect(200)
        .end(function(err, res){
            if(err){
                done(err);
            }else{
                done();
            }
        });
    });
});
it("No debería de obtener un título 'Express'", (done)=>{
    supertest(app).get("/index/1")
    .expect(403)
    .end(function(err, res){
        if(err){
            done(err);
        }else{
            done();
        }
    });
});