const supertest = require('supertest');
const app = require('../app');

describe("Probar el añadir una burnDownChart", ()=>{
    it("Debería de obtener un burnDownChart añadido", (done)=>{
        supertest(app).post("/burnDownChart")
        .expect(200)
        .end(function(err, res){
            if(err){
                done(err);
            }else{
                done();
            }
        });
    });
});
it("No debería de obtener una backcard añadido", (done)=>{
    supertest(app).post("/burnDownChart/1")
    .expect(403)
    .end(function(err, res){
        if(err){
            done(err);
        }else{
            done();
        }
    });
});

describe("Probar el obtener todas las burnDownCharts", ()=>{
    it("Debería de obtener una lista de burnDownCharts", (done)=>{
        supertest(app).get("/burnDownChart")
        .expect(200)
        .end(function(err, res){
            if(err){
                done(err);
            }else{
                done();
            }
        });
    });
});
it("No debería de obtener una lista de burnDownCharts", (done)=>{
    supertest(app).get("/burnDownChart/2d")
    .expect(403)
    .end(function(err, res){
        if(err){
            done(err);
        }else{
            done();
        }
    });
});