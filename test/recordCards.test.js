const supertest = require('supertest');
const app = require('../app');

describe("Probar el añadir un reporte", ()=>{
    it("Debería de obtener un reporte añadido", (done)=>{
        supertest(app).post("/recordCards")
        .expect(200)
        .end(function(err, res){
            if(err){
                done(err);
            }else{
                done();
            }
        });
    });
});
it("No debería de obtener un reporte añadido", (done)=>{
    supertest(app).post("/recordCards/1")
    .expect(403)
    .end(function(err, res){
        if(err){
            done(err);
        }else{
            done();
        }
    });
});

describe("Probar el obtener todos los reportes", ()=>{
    it("Debería de obtener una lista de reportes", (done)=>{
        supertest(app).get("/recordCards")
        .expect(200)
        .end(function(err, res){
            if(err){
                done(err);
            }else{
                done();
            }
        });
    });
});
it("No debería de obtener una lista de reportes", (done)=>{
    supertest(app).get("/recordCards/2d")
    .expect(403)
    .end(function(err, res){
        if(err){
            done(err);
        }else{
            done();
        }
    });
});