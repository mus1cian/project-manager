const supertest = require('supertest');
const app = require('../app');

describe("Probar el añadir un miembro del equipo", ()=>{
    it("Debería de obtener un miembro añadido", (done)=>{
        supertest(app).post("/teamMembers/")
        .expect(200)
        .end(function(err, res){
            if(err){
                done(err);
            }else{
                done();
            }
        });
    });
});
it("No debería de obtener un miembro añadido", (done)=>{
    supertest(app).post("/teamMembers/1")
    .expect(403)
    .end(function(err, res){
        if(err){
            done(err);
        }else{
            done();
        }
    });
});

describe("Probar el obtener todos los miembros", ()=>{
    it("Debería de obtener una lista de miembros del equipo", (done)=>{
        supertest(app).get("/teamMembers")
        .expect(200)
        .end(function(err, res){
            if(err){
                done(err);
            }else{
                done();
            }
        });
    });
});
it("No debería de obtener una lista de miembros del equipo", (done)=>{
    supertest(app).get("/teamMembers/2d")
    .expect(403)
    .end(function(err, res){
        if(err){
            done(err);
        }else{
            done();
        }
    });
});