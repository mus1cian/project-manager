const supertest = require('supertest');
const app = require('../app');

describe("Probar el añadir miembro del equipo", ()=>{
    it("debería de obtener un usuario añadido", (done)=>{
        supertest(app).patch("/records/add/teamMember/1")
        .send({'idTeamMember': '5554'})
        .expect(200)
        .end(function(err, res){
            if(err){
                done(err);
            }else{
                done();
            }
        });
    });
});

it("No debería de obtener un usuario añadido", (done)=>{
    supertest(app).patch("/records/add/teamMember/1")
    .send({'idTeamMember': 'm554'})
    .expect(403)
    .end(function(err, res){
        if(err){
            done(err);
        }else{
            done();
        }
    });
});
describe("Probar el añadir un project manager", ()=>{
    it("debería de obtener un usuario añadido", (done)=>{
        supertest(app).patch("/records/add/projectManager/1")
        .send({'idTeamMember': '5554'})
        .expect(200)
        .end(function(err, res){
            if(err){
                done(err);
            }else{
                done();
            }
        });
    });
});

it("No debería de obtener un usuario añadido", (done)=>{
    supertest(app).patch("/records/add/projectManager/1")
    .send({'idTeamMember': 'm554'})
    .expect(403)
    .end(function(err, res){
        if(err){
            done(err);
        }else{
            done();
        }
    });
});
describe("Probar el añadir un project owner", ()=>{
    it("debería de obtener un usuario añadido", (done)=>{
        supertest(app).patch("/records/add/projectOwner/1")
        .send({'idTeamMember': '5554'})
        .expect(200)
        .end(function(err, res){
            if(err){
                done(err);
            }else{
                done();
            }
        });
    });
});

it("No debería de obtener un usuario añadido", (done)=>{
    supertest(app).patch("/records/add/projectOwner/1")
    .send({'idTeamMember': 'm554'})
    .expect(403)
    .end(function(err, res){
        if(err){
            done(err);
        }else{
            done();
        }
    });
});