const supertest = require('supertest');
const app = require('../app');

describe("Probar el añadir una backCard", ()=>{
    it("Debería de obtener un backCard añadido", (done)=>{
        supertest(app).post("/backCard")
        .expect(200)
        .end(function(err, res){
            if(err){
                done(err);
            }else{
                done();
            }
        });
    });
});
it("No debería de obtener una backcard añadido", (done)=>{
    supertest(app).post("/backCard/1")
    .expect(403)
    .end(function(err, res){
        if(err){
            done(err);
        }else{
            done();
        }
    });
});

describe("Probar el obtener todas las backCards", ()=>{
    it("Debería de obtener una lista de backCards", (done)=>{
        supertest(app).get("/backCard")
        .expect(200)
        .end(function(err, res){
            if(err){
                done(err);
            }else{
                done();
            }
        });
    });
});
it("No debería de obtener una lista de backCards", (done)=>{
    supertest(app).get("/backCard/2d")
    .expect(403)
    .end(function(err, res){
        if(err){
            done(err);
        }else{
            done();
        }
    });
});