const supertest = require('supertest');
const app = require('../app');

describe("Probar el añadir un permiso", ()=>{
    it("Debería de obtener un permiso añadido", (done)=>{
        supertest(app).post("/permission")
        .expect(200)
        .end(function(err, res){
            if(err){
                done(err);
            }else{
                done();
            }
        });
    });
});
it("No debería de obtener un permiso añadido", (done)=>{
    supertest(app).post("/permission/1")
    .expect(403)
    .end(function(err, res){
        if(err){
            done(err);
        }else{
            done();
        }
    });
});

describe("Probar el obtener todos los permisos", ()=>{
    it("Debería de obtener una lista de permisos", (done)=>{
        supertest(app).get("/recordCard")
        .expect(200)
        .end(function(err, res){
            if(err){
                done(err);
            }else{
                done();
            }
        });
    });
});
it("No debería de obtener una lista de permisos", (done)=>{
    supertest(app).get("/recordCard/2d")
    .expect(403)
    .end(function(err, res){
        if(err){
            done(err);
        }else{
            done();
        }
    });
});