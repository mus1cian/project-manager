var express = require('express');
var router = express.Router();

const controller = require('../controllers/backCard');

router.get('/', controller.getBackCards);

router.get('/:id', controller.getBackCard);

router.post('/', controller.createBackCard);

router.put('/:id', controller.replaceBackCard);

router.patch('/:id', controller.updateBackCard);

router.delete('/:id', controller.destroyBackCard);

module.exports = router;