var express = require('express');
var router = express.Router();

const controller = require('../controllers/permissions');

router.get('/', controller.getPermissions);

router.get('/:id', controller.getPermission);

router.post('/', controller.createPermission);

router.put('/:id', controller.replacePermission);

router.patch('/:id', controller.updatePermission);

router.delete('/:id', controller.destroyPermission);

module.exports = router;