var express = require('express');
var router = express.Router();

const controller = require('../controllers/address');

router.get('/', controller.getAddresses);

router.get('/:id', controller.getAddress);

router.post('/', controller.createAddress);

router.put('/:id', controller.replaceAddress);

router.patch('/:id', controller.updateAddress);

router.delete('/:id', controller.destroyAddress);

module.exports = router;