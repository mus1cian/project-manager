var express = require('express');
var router = express.Router();

const controller = require('../controllers/burnDownChart');

router.get('/', controller.getBurnDownCharts);

router.get('/:id', controller.getBurnDownChart);

router.post('/', controller.createBurnDownChart);

router.put('/:id', controller.replaceBurnDownChart);

router.patch('/:id', controller.updateBurnDownChart);

router.delete('/:id', controller.destroyBurnDownChart);

module.exports = router;