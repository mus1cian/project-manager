var express = require('express');
var router = express.Router();

const controller = require('../controllers/frontCard');

router.get('/', controller.getFrontCards);

router.get('/:id', controller.getFrontCard);

router.post('/', controller.createFrontCard);

router.put('/:id', controller.replaceFrontCard);

router.patch('/:id', controller.updateFrontCard);

router.delete('/:id', controller.destroyFrontCard);

module.exports = router;