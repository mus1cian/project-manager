var express = require('express');
var router = express.Router();

const controller = require("../controllers/records")

/* GET records listing. */
router.get('/', controller.list);

router.get('/:id', controller.index);

router.post('/', controller.create);

router.put('/:id', controller.replace);

router.patch('/:id', controller.update);

router.patch('/add/teamMember/:id', controller.addTeamMember);

router.patch('/add/projectManager/:id', controller.addProjectManager);

router.patch('/add/projectOwner/:id', controller.addProjectOwner);

router.delete('/:id', controller.destroy);

module.exports = router;
