var express = require('express');
var router = express.Router();

const controller = require('../controllers/profiles');

router.get('/', controller.getProfiles);

router.get('/:id', controller.getProfile);

router.post('/', controller.createProfile);

router.put('/:id', controller.replaceProfile);

router.patch('/:id', controller.updateProfile);

router.delete('/:id', controller.destroyProfile);

module.exports = router;