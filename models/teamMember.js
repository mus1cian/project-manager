const mongoose = require('mongoose');

const schema = mongoose.Schema({
    _record_id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Record",
    },
    _name: String,
    _birthDate: String,
    _curp: String,
    _rfc: String,
    _email: String,
    _password: String,
    _salt: String,
    _address: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Address",
    },
    _skills: [{
        type: String, enum: ['JUNIOR','SENIOR','MASTER']
    }, {
      type: String, enum: ['JAVA','PYTHON','C++', 'C#']
    }],
});

class TeamMember {
  constructor(member_id, name, birthDate, curp, rfc, address, skills, email) {
    this._member_id = member_id;
    this._name = name;
    this._birthDate = birthDate;
    this._curp = curp;
    this._rfc = rfc;
    this._address = address;
    this._skills = skills;
    this._email = email;
  }
  get recordId(){ return this._member_id; }
  set recordId(member_id){ this._member_id = member_id; }
  get projectName(){ return this._name; }
  set projectName(name) { this._name = name; }
  get requestDate(){ return this._birthDate; }
  set requestDate(birthDate) { this._birthDate = birthDate}
  get startDate(){ return this._curp; }
  set startDate(curp) { this._curp = curp; }
  get projectDescription(){ return this._rfc; }
  set projectDescription(rfc) { this._rfc = rfc; }
  get projectManager() { return this._address; }
  set projectManager(address) { this._address = address; }
  get projectOwner() { return this._skills; }
  set projectOwner(skills) { this._skills = skills; }
  set email(email) { this._email = email;}
  get email() { return this._email; }
  set password(password) { this._password = password; }
  get password() { return this._password; }
  set salt(salt) { this._salt = salt; }
  get salt() { return this._salt; }
  set address(address) { this._address = address; }
  get address() { return this._address; }
  set skills(skills) { this._skills = skills; }
  get skills() { return this._skills; }
}

schema.loadClass(TeamMember);
module.exports = mongoose.model('teamMember', schema);