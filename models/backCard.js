const mongoose = require('mongoose');

const schema = mongoose.Schema({
    _criteria: String,
    _context: String,
    _events: [{
        type: String
    }],
    _results: [{
        type: String
    }]

});

class BackCard {
  constructor(criteria, context, events, results) {
    this.criteria = criteria;
    this.context = context;
    this.events = events;
    this.results = results;

}
    get criteria() { return this.criteria;}
    set criteria(value) { this.criteria = value; }
    get context() { return this.context; }
    set context(value) { this.context = value; }
    get events() { return this.events; }
    set events(value) { this.events = value; }
    get results() { return this.results; }
    set results(value) { this.results = value; }

}

schema.loadClass(BackCard);
module.exports = mongoose.model('BackCard', schema);