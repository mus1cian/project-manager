const mongoose = require('mongoose');

const schema = mongoose.Schema({
    _projectName: String,
    _requestDate: String,
    _startDate: String,
    _projectDescription: String,
    _projectManager: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "TeamMember"
    },
    _projectOwner: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "TeamMember"
    },
    _devTeam: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: "TeamMember"
    }]
});

class Permission {
  constructor(projectName, requestDate, startDate, proyectDescription, projectManager, projectOwner, devTeam) {
    this._projectName = projectName;
    this._requestDate = requestDate;
    this._startDate = startDate;
    this._projectDescription = proyectDescription;
    this._projectManager = projectManager;
    this._projectOwner = projectOwner;
    this.devTeam = devTeam;
  }
  get recordId(){ return this._recordId; }
  set recordId(recordId){ this._recordId = recordId; }
  get projectName(){ return this._projectName; }
  set projectName(projectName) { this._projectName = projectName; }
  get requestDate(){ return this._requestDate; }
  set requestDate(requestDate) { this._requestDate = requestDate}
  get startDate(){ return this._startDate; }
  set startDate(startDate) { this._startDate = startDate; }
  get projectDescription(){ return this._projectDescription; }
  set projectDescription(projectDescription) { this._projectDescription = projectDescription; }
  get projectManager() { return this._projectManager; }
  set projectManager(projectManager) { this.projectManager = projectManager; }
  get projectOwner() { return this._projectOwner; }
  set projectOwner(projectOwner) { this.projectOwner = projectOwner; }
  get devteam() { return this._devTeam; }
  set devteam(devteam) { this._devTeam = devteam; }
}

schema.loadClass(Record);
module.exports = mongoose.model('record', schema);