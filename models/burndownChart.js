const mongoose = require('mongoose');

const schema = mongoose.Schema({
    _recordCard: [
        {
          type: mongoose.Schema.Types.ObjectId,
          ref: "RecordCard",
        },
      ],
    _sprints: Number,
    _storyPoints: Number
});

class BurnDownChart {
  constructor(recordCard, sprints, storyPoints) {
    this.recordCard = recordCard;
    this.sprints = sprints;
    this.storyPoints = storyPoints;

}
    get recordCard() { return this.record}
    set recordCard(record) { this.record = record;}
    get sprints() { return this.sprints}
    set sprints(sprints) { this.sprints = sprints;}
    get storyPoints() { return this.storyPoints}
    set storyPoints(storyPoints) { this.storyPoints = storyPoints;}
}

schema.loadClass(BurnDownChart);
module.exports = mongoose.model('BurnDownChart', schema);