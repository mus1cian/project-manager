const mongoose = require('mongoose');

const schema = mongoose.Schema({
    _record: [
        {
          type: mongoose.Schema.Types.ObjectId,
          ref: "Record",
        },
      ],
    _frontCard: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "FrontCard",
        },
    ],
    _backCard: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "BackCard",
        },
    ],
    _value: Number,
    _column: {
        type: String, enum: ['PRODUCT','RELEASE','SPRINT']
      }
    
});

class RecordCard {
  constructor(record, frontCard, backCard, value, column) {
    this.record = record;
    this.frontCard = frontCard;
    this.backCard = backCard;
    this.value = value;
    this.column = column;

}
    get record() { return this.record; }
    set record(val) { this.record = val; }
    get frontCard() { return this.frontCard; }
    set frontCard(val) { this.frontCard = val; }
    get backCard() { return this.backCard; }
    set backCard(val) { this.backCard = val; }
    get value() { return this.value; }
    set value(val) { this.value = val; }
    get column() { return this.column; }
    set column(val) { this.column = val; }
    
}

schema.loadClass(RecordCard);
module.exports = mongoose.model('RecordCard', schema);