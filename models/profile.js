const mongoose = require('mongoose');
const Permission = require('./permission.js');

const schema = mongoose.Schema({
  _description: String,
  _status: Boolean,
  _permissions: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Permission'
  }],
});

class Profile {
  constructor(description, status,permissions) {
    this._description = description;
    this._status = status;
    this.permissions = permissions;
  }

  get description(){
    return this._description;
  }

  set description(v){
    this._description = v;
  }

  get status(){
    return this._status;
  }

  set status(v){
    this._status = v;
  }

  get permissions(){
    return this._permissions;
  }

  set permissions(v){
    this._permissions = v;
  }

}

schema.loadClass(Profile);
module.exports = mongoose.model('Profile', schema);