const mongoose = require('mongoose');

const schema = mongoose.Schema({
    _priority: String,
    _size: String,
    _title: String,
    _role: String,
    _functionality: String,
    _benefit: String
});

class FrontCard {
  constructor(priority, size, title, role, functionality, benefit) {
    this._priority = priority;
    this._size = size;
    this._title = title;
    this._role = role;
    this._functionality = functionality;
    this._benefit = benefit;

}
    get title() { return this._title; }
    get priority() { return this._priority; }
    get size() { return this._size; }
    get role() { return this._role; }
    get functionality() { return this._functionality; }
    get benefit() { return this._benefit; }

    set title(title) { this._title = title; }
    set priority(priority) { this._priority = priority; }
    set size(size) { this._size = size; }
    set role(role) { this._role = role; }
    set functionality(functionality) { this._functionality = functionality; }
    set benefit(benefit) { this._benefit = benefit; }
}

schema.loadClass(FrontCard);
module.exports = mongoose.model('FrontCard', schema);