const mongoose = require('mongoose');

const schema = mongoose.Schema({
    _street: String,
    _city: String,
    _number: String,
    _zip: String,
    _state: String,
});

class Permission {
  constructor(street, city, number, zip, state) {
    this._street = street;
    this._city = city;
    this._number = number;
    this._zip = zip;
    this._state = state;
  }
    get street() { return this._street; }
    set street(street) { this._street = street;}
    get city() { return this._city; }
    set city(city) { this._city = city; }
    get zip() { return this._zip; }
    set zip(zip) { this._zip = zip; }
    get state() { return this._state; }
    set state(state) { return this._state = state; }
}

schema.loadClass(Permission);
module.exports = mongoose.model('Permission', schema);