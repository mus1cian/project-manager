const express = require('express');
const { configure } = require('../rbac');

// method url action

// GET  list
function getProfiles(req, res, next) {
    req.ability.then((ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("READ", Profile);
            Profile.find().populate("_profiles")
                .then(objs => res.status(200).json({
                    message: res.__('ok.getProfiles'),
                    obj: objs
                }))
                .catch(ex => res.status(500).json({
                    message: res.__('bad.getProfiles'),
                    obj: ex
            }));
        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    })
};

// GET /{id} index
function getProfile(req, res, next) {
    req.ability.then((ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("READ", Profile);
            const id = req.params.id;
            Profile.findOne({"_id": id})
            .then(obj => res.status(200).json({
                message: res.__('ok.getProfile'),
                obj: obj
            }))
            .catch(ex => res.status(500).json({
                message: res.__('bad.getProfile'),
                obj: ex
            }));
        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    })
};

// POST create
function createProfile(req, res, next) {
    req.ability.then((ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("CREATE", Profile);
            const description = req.body.description;
            const status = req.body.status;

            let profile = new Profile({
                description: description,
                status: status,
                permissions: []
            });

            profile
                .save()
                .then((obj) =>
                res.status(200).json({
                    message: res.__('ok.createProfile'),
                    obj: obj,
                })
                )
                .catch((ex) =>
                res.status(500).json({
                    message: res.__('bad.createProfile'),
                    obj: ex,
                })
                );

        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    })
    
};


function replaceProfile(req, res, next) {
    req.ability.then(async (ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("UPDATE", Profile);
            const id = req.params.id;
            const description = req.body.description ? req.body.description: "";
            const status = req.body.status ? req.body.status: false;
            const permissionsId = req.body.permissionsId ? req.body.permissionsId: "";

            let permissions = await Permission.find({"_id": permissionsId});

            let profile = new Object({
                _description: description,
                _status: status,
                _permissions: permissions
            });

            Profile.findOneAndUpdate({"_id": id}, profile, { new: true })
                .then(obj => res.status(200).json({
                    message: res.__('ok.replaceProfile'),
                    obj: obj
                }))
                .catch(ex => res.status(500).json({
                    message: res.__('bad.replaceProfile'),
                    obj: ex
                }));

        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    })
    
};

// PATCH /{id} update
function updateProfile(req, res, next) {
    req.ability.then(async (ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("UPDATE", Profile);
            const id = req.params.id;
            const description = req.body.description;
            const status = req.body.status;
            const permissionsId = req.body.permissionsId;

            let permissions = await Permission.find({"_id": permissionsId});
            
            let profile = new Object();

            if(description) profile._description = description;
            if(status) profile._status = status;
            if(permissions) profile._permissions = permissions;

            Profile.findOneAndUpdate({"_id": id}, profile, { new: true })
                .then(obj => res.status(200).json({
                    message: res.__('ok.updateProfile'),
                    obj: obj
                }))
                .catch(ex => res.status(500).json({
                    message: res.__('bad.updateProfile'),
                    obj: ex
                }));

        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    })
    
};


// DELETE /{id} destroy
function destroyProfile(req, res, next) {
    req.ability.then((ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("DELETE", Profile);
            const id = req.params.id;
            Profile.remove({"_id": id})
                .then(obj => res.status(200).json({
                    message: res.__('ok.destroyProfile'),
                    obj: obj
                }))
                .catch(ex => res.status(500).json({
                    message: res.__('bad.destroyProfile'),
                    obj: ex
                }));
        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    })
};

module.exports = {
    getProfiles,
    getProfile,
    createProfile,
    replaceProfile,
    updateProfile,
    destroyProfile
};

