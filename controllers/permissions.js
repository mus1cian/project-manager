const express = require('express');
const { configure } = require('../rbac');

// method url action

// GET  list
function getPermissions(req, res, next) {
    req.ability.then((ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("READ", Permission);
            Permission.find().populate("_permissions")
                .then(objs => res.status(200).json({
                    message: res.__('ok.getPermissions'),
                    obj: objs
                }))
                .catch(ex => res.status(500).json({
                    message: res.__('bad.getPermissions'),
                    obj: ex
            }));
        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    })
};

// GET /{id} index
function getPermission(req, res, next) {
    req.ability.then((ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("READ", Permission);
            const id = req.params.id;
            Permission.findOne({"_id": id})
            .then(obj => res.status(200).json({
                message: res.__('ok.getPermission'),
                obj: obj
            }))
            .catch(ex => res.status(500).json({
                message: res.__('bad.getPermission'),
                obj: ex
            }));
        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    })
};

// POST create
function createPermission(req, res, next) {
    req.ability.then((ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("CREATE", Permission);
            const description = req.body.description;
            const type = req.body.type;
            
            let permission = new Permission({
                description: description,
                type: type
            });

            permission
                .save()
                .then((obj) =>
                res.status(200).json({
                    message: res.__('ok.createPermission'),
                    obj: obj,
                })
                )
                .catch((ex) =>
                res.status(500).json({
                    message: res.__('bad.createPermission'),
                    obj: ex,
                })
                );

        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    })
};


// PUT /{id} replace
function replacePermission(req, res, next) {
    req.ability.then(async (ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("UPDATE", Permission);
            const id = req.params.id;
            const description = req.body.description ? req.body.description: "";
            const type = req.body.type ? req.body.type: "";
            
            let permission = new Permission({
                description: description,
                type: type
            });

            Permission.findOneAndUpdate({"_id": id}, permission, { new: true })
                .then(obj => res.status(200).json({
                    message: res.__('ok.replacePermission'),
                    obj: obj
                }))
                .catch(ex => res.status(500).json({
                    message: res.__('bad.replacePermission'),
                    obj: ex
                }));

        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    })
};

// PATCH /{id} update
function updatePermission(req, res, next) {
    req.ability.then(async (ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("UPDATE", Permission);
            const description = req.body.description;
            const type = req.body.type;
            
            let permission = new Object();

            if(description) permission._description = description;
            if(type) permission._type = type;

            Permission.findOneAndUpdate({"_id": id}, permission, { new: true })
                .then(obj => res.status(200).json({
                    message: res.__('ok.updatePermission'),
                    obj: obj
                }))
                .catch(ex => res.status(500).json({
                    message: res.__('bad.updatePermission'),
                    obj: ex
                }));

        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    })
};


// DELETE /{id} destroy
function destroyPermission(req, res, next) {
    req.ability.then((ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("DELETE", Permission);
            const id = req.params.id;
            Permission.remove({"_id": id})
                .then(obj => res.status(200).json({
                    message: res.__('ok.destroyPermission'),
                    obj: obj
                }))
                .catch(ex => res.status(500).json({
                    message: res.__('bad.destroyPermission'),
                    obj: ex
                }));
        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    })
};


module.exports = {
    getPermissions,
    getPermission,
    createPermission,
    replacePermission,
    updatePermission,
    destroyPermission
};

