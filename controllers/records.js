const express = require('express');

// method url action

// GET  list
function getRecords(req, res, next) {
    req.ability.then((ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("READ", Record);
            Record.find().populate("_records")
            .then(objs => res.status(200).json({
                message: res.__('ok.getRecords'),
                obj: objs
            }))
            .catch(ex => res.status(500).json({
                message: res.__('bad.getRecords'),
                obj: ex
        }));
        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    });
};

// GET /{id} index
function getRecord(req, res, next) {
    req.ability.then((ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("READ", Record);
            const id = req.params.id;
            Record.findOne({"_id": id})
            .then(obj => res.status(200).json({
                message: res.__('ok.getRecord'),
                obj: obj
            }))
            .catch(ex => res.status(500).json({
                message: res.__('bad.getRecord'),
                obj: ex
            }));
        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    });
};

function getProjectManager(req, res, next) {
    req.ability.then((ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("READ", Record);
            const id = req.params.id;
            RecordCard.findOne({"_projectManager": id})
            .then(obj => res.status(200).json({
                message: res.__('ok.getRecordCard'),
                obj: obj
            }))
            .catch(ex => res.status(500).json({
                message: res.__('bad.getRecord'),
                obj: ex
            }));
        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    });
    
}

function getProjectOwner(req, res, next) {
    req.ability.then((ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("READ", Record);
            const id = req.params.id;
            RecordCard.findOne({"_projectOwner": id})
            .then(obj => res.status(200).json({
                message: res.__('ok.getProjectOwner'),
                obj: obj
            }))
            .catch(ex => res.status(500).json({
                message: res.__('bad.getProjectOwner'),
                obj: ex
            }));
        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    });
}

// POST create
function createRecord(req, res, next) {
    req.ability.then(async (ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("CREATE", Record);
            const projectName = req.body.projectName;
            const requestDate = req.body.requestDate;
            const startDate = req.body.startDate;
            const projectDescription = req.body.projectDescription;
            const projectManagerId = req.body.projectManagerId;
            const projectOwnerId = req.body.projectOwnerId;
            const devTeam = [];

            let projectManager = await TeamMember.find({"_id": projectManagerId});
            let projectOwner = await TeamMember.find({"_id": projectOwnerId});

            let record = new Record({
                projectName: projectName,
                requestDate: requestDate,
                startDate: startDate,
                projectDescription: projectDescription,
                projectManager: projectManager,
                projectOwner: projectOwner,
                devTeam: devTeam
            });

            record
                .save()
                .then((obj) =>
                res.status(200).json({
                    message: res.__('ok.createRecordCard'),
                    obj: obj,
                })
                )
                .catch((ex) =>
                res.status(500).json({
                    message: res.__('bad.createRecordCard'),
                    obj: ex,
                })
                );
        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    });
};

function addDeveloper(req, res, next) {
    req.ability.then(async (ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("CREATE", Record);
            const id = req.params.id;
            const memberId = req.params.id;

            let developer = await TeamMember.findOne({_id: id});

            Record.findByIdAndUpdate(
                { _id: id },
                { $push: { _devTeam: developer } },
                { upsert: true, new: true }
                )
            
                .then((obj) =>
                res.status(200).json({
                    message: res.__('ok.addDeveloper'),
                    obj: obj,
                })
                )
                .catch((ex) =>
                res.status(500).json({
                    message: res.__('bad.addDeveloper'),
                    obj: ex,
                })
                );

        }catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
            
});
}

function removeDeveloper(req, res, next) {
    req.ability.then(async (ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("DELETE", Record);
            const id = req.params.id;
            const memberId = req.params.id;

            let developer = await TeamMember.findOne({_id: id});

            Record.findByIdAndUpdate(
                { _id: id },
                { $pull: { _devTeam: developer } },
                { upsert: true, new: true }
              )
            
                .then((obj) =>
                res.status(200).json({
                    message: res.__('ok.removeDeveloper'),
                    obj: obj,
                })
                )
                .catch((ex) =>
                res.status(500).json({
                    message: res.__('bad.removeDeveloper'),
                    obj: ex,
                })
                );

        }catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
        
});
}


function addProjectManager(req, res, next) {
    req.ability.then(async (ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("UPDATE", Record);
            const id = req.params.id;
            const memberId = req.params.id;

            let record = await Record.findOne({_id: id});

            record._projectManager = memberId;

            Record.findOneAndUpdate({ _id: id }, record, { new: true })
            .then((obj) =>
            res.status(200).json({
                message: res.__('ok.addProjectManager'),
                obj: obj,
            })
            )
            .catch((ex) =>
            res.status(500).json({
                message: res.__('bad.addProjectManager'),
                obj: ex,
            })
            );

        }catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
            
    });
}

function addProjectOwner(req, res, next) {
    req.ability.then(async (ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("UPDATE", Record);
            const id = req.params.id;
            const memberId = req.params.id;

            let record = await Record.findOne({_id: id});

            record._projectOwner = memberId;

            Record.findOneAndUpdate({ _id: id }, record, { new: true })
            .then((obj) =>
            res.status(200).json({
                message: res.__('ok.addProjectOwner'),
                obj: obj,
            })
            )
            .catch((ex) =>
            res.status(500).json({
                message: res.__('bad.addProjectOwner'),
                obj: ex,
            })
            );

        }catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
            
    });
}

// PUT /{id} replace
function replaceRecord(req, res, next) {
    req.ability.then(async (ability) => {
        try {
          ForbiddenError.from(ability).throwUnlessCan("UPDATE", User);
          const id = req.params.id;
    
            const projectName = req.body.projectName ? req.body.projectName: "";
            const requestDate = req.body.requestDate ? req.body.requestDate: "";
            const startDate = req.body.startDate ? req.body.startDate: "";
            const projectDescription = req.body.projectDescription ? req.body.projectDescription: "";
            const projectManagerId = req.body.projectManagerId? req.body.projectManager: "";
            const projectOwnerId = req.body.projectOwnerId? req.body.projectOwner: "";

            let projectManager = await TeamMember.find({"_id": projectManagerId});
            let projectOwner = await TeamMember.find({"_id": projectOwnerId});
            

          let record = new Record({
            projectName: projectName,
            requestDate: requestDate,
            startDate: startDate,
            projectDescription: projectDescription,
            projectManager: projectManager,
            projectOwner: projectOwner,
            devTeam: devTeam
        });
    
    
          Record.findOneAndUpdate({ _id: id }, record, { new: true })
            .then((obj) =>
              res.status(200).json({
                message: res.__('ok.updateRecord'),
                obj: obj,
              })
            )
            .catch((ex) =>
              res.status(500).json({
                message: res.__('bad.updateRecord'),
                obj: ex,
              })
            );
        } catch (error) {
          if (error instanceof ForbiddenError) {
            res.status(500).json({
              message: "ERROR WITH PERMISSIONS",
              obj: error.message,
            });
          }
        }
      });
    
};

// PATCH /{id} update
function updateRecord(req, res, next) {
    req.ability.then(async (ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("UPDATE", TeamMember);
            const id = req.params.id;
    
            const projectName = req.body.projectName;
            const requestDate = req.body.requestDate;
            const startDate = req.body.startDate;
            const projectDescription = req.body.projectDescription;
            const projectManagerId = req.body.projectManagerId;
            const projectOwnerId = req.body.projectOwnerId;

            let projectManager = undefined;
            let projectOwner = undefined;

            if (projectManagerId) {
                projectManager = await TeamMember.find({"_id": projectManagerId});
            }

            if (projectOwnerId) {
                projectOwner = await TeamMember.find({"_id": projectOwnerId});
            }
            

          let record = new Record({
            projectName: projectName,
            requestDate: requestDate,
            startDate: startDate,
            projectDescription: projectDescription,
            projectManager: projectManager,
            projectOwner: projectOwner,
            devTeam: devTeam
        });
    
    
          Record.findOneAndUpdate({ _id: id }, record, { new: true })
            .then((obj) =>
              res.status(200).json({
                message: res.__('ok.updateRecord'),
                obj: obj,
              })
            )
            .catch((ex) =>
              res.status(500).json({
                message: res.__('bad.updateRecord'),
                obj: ex,
              })
            );
        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    });
};

// DELETE /{id} destroy
function destroyRecord(req, res, next) {
    req.ability.then((ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("DELETE", Record);
            const id = req.params.id;
            Record.remove({"_id": id})
                .then(obj => res.status(200).json({
                    message: res.__('ok.destroyRecord'),
                    obj: obj
                }))
                .catch(ex => res.status(500).json({
                    message: res.__('bad.destroyRecord'),
                    obj: ex
                }));
        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    });
};



module.exports = {
    getRecords,
    getRecord,
    createRecord,
    replaceRecord,
    updateRecord,
    destroyRecord,
    addDeveloper,
    removeDeveloper,
    addProjectManager,
    addProjectOwner,
    getProjectManager,
    getProjectOwner,
};
