const express = require('express');
const { configure } = require('../rbac');

// method url action

// GET  list
function getAddresses(req, res, next) {
    req.ability.then((ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("READ", Address);
            Address.find().populate("_addresses")
                .then(objs => res.status(200).json({
                    message: res.__('ok.getAddresses'),
                    obj: objs
                }))
                .catch(ex => res.status(500).json({
                    message: res.__('bad.getAddresses'),
                    obj: ex
            }));
        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    })
};

// GET /{id} index
function getAddress(req, res, next) {
    req.ability.then((ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("READ", Address);
            const id = req.params.id;
            Address.findOne({"_id": id})
            .then(obj => res.status(200).json({
                message: res.__('ok.getAddress'),
                obj: obj
            }))
            .catch(ex => res.status(500).json({
                message: res.__('bad.getAddress'),
                obj: ex
            }));
        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    })
};

// POST create
function createAddress(req, res, next) {
    req.ability.then((ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("CREATE", Address);
            const street = req.body.street;
            const city = req.body.city;
            const number = req.body.number;
            const zip = req.body.zip;
            const state = req.body.state;

            let address = new Address({
                street: street,
                city: city,
                number: number,
                zip: zip,
                state: state,
            });

            address
                .save()
                .then((obj) =>
                res.status(200).json({
                    message: res.__('ok.createAddress'),
                    obj: obj,
                })
                )
                .catch((ex) =>
                res.status(500).json({
                    message: res.__('bad.createAddress'),
                    obj: ex,
                })
                );

        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    })
};


// PUT /{id} replace
function replaceAddress(req, res, next) {
    req.ability.then(async (ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("UPDATE", Address);
            const id = req.params._id;
            const street = req.body.street ? req.body.street: "";
            const city = req.body.street ? req.body.street: "";
            const number = req.body.street ? req.body.street: "";
            const zip = req.body.street ? req.body.street: "";
            const state = req.body.street ? req.body.street: "";

            let address = new Object({
                _street: street,
                _city: city,
                _number: number,
                _zip: zip,
                _state: state,
            });

            Address.findOneAndUpdate({"_id": id}, address, { new: true })
                .then(obj => res.status(200).json({
                    message: res.__('ok.replaceAddress'),
                    obj: obj
                }))
                .catch(ex => res.status(500).json({
                    message: res.__('bad.replaceAddress'),
                    obj: ex
                }));

        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    })
};

// PATCH /{id} update
function updateAddress(req, res, next) {
    req.ability.then(async (ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("UPDATE", Address);
            const id = req.body.id;
            const street = req.body.street;
            const city = req.body.city;
            const number = req.body.number;
            const zip = req.body.zip;
            const state = req.body.state;
            
            let address = new Object();

            if(street) address._street = street;
            if(city) address._city = city;
            if(number) address._number = number;
            if(zip) address._zip = zip;
            if(state) address._state = state;

            Address.findOneAndUpdate({"_id": id}, address, { new: true })
                .then(obj => res.status(200).json({
                    message: res.__('ok.updateAddress'),
                    obj: obj
                }))
                .catch(ex => res.status(500).json({
                    message: res.__('bad.updateAddress'),
                    obj: ex
                }));

        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    })
};


// DELETE /{id} destroy
function destroyAddressCard(req, res, next) {
    req.ability.then((ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("DELETE", Address);
            const id = req.params.id;
            Address.remove({"_id": id})
                .then(obj => res.status(200).json({
                    message: res.__('ok.destroyAddress'),
                    obj: obj
                }))
                .catch(ex => res.status(500).json({
                    message: res.__('bad.destroyAddress'),
                    obj: ex
                }));
        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    })
};


module.exports = {
    getAddresses,
    getAddress,
    createAddress,
    replaceAddress,
    updateAddress,
    destroyAddressCard
};
