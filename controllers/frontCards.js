const express = require('express');
const { configure } = require('../rbac');

// method url action

// GET  list
function getFrontCards(req, res, next) {
    req.ability.then((ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("READ", FrontCard);
            FrontCard.find().populate("_frontCards")
                .then(objs => res.status(200).json({
                    message: res.__('ok.getFrontCards'),
                    obj: objs
                }))
                .catch(ex => res.status(500).json({
                    message: res.__('bad.getFrontCards'),
                    obj: ex
            }));
        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    })
};

// GET /{id} index
function getFrontCard(req, res, next) {
    req.ability.then((ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("READ", FrontCard);
            const id = req.params.id;
            FrontCard.findOne({"_id": id})
            .then(obj => res.status(200).json({
                message: res.__('ok.getFrontCard'),
                obj: obj
            }))
            .catch(ex => res.status(500).json({
                message: res.__('bad.getFrontCard'),
                obj: ex
            }));
        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    })
};

// POST create
function createFrontCard(req, res, next) {
    req.ability.then((ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("CREATE", FrontCard);
            const priority = req.body.priority;
            const size = req.body.size;
            const title = req.body.title;
            const role = req.body.role;
            const functionality = req.body.functionality;
            const benefit = req.body.benefit;

            let frontCard = new FrontCard({
                priority: priority,
                size: size,
                title: title,
                role: role,
                functionality: functionality,
                benefit: benefit
            });

            frontCard
                .save()
                .then((obj) =>
                res.status(200).json({
                    message: res.__('ok.createFrontCard'),
                    obj: obj,
                })
                )
                .catch((ex) =>
                res.status(500).json({
                    message: res.__('bad.createFrontCard'),
                    obj: ex,
                })
                );

        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    })
};


// PUT /{id} replace
function replaceFrontCard(req, res, next) {
    req.ability.then(async (ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("UPDATE", FrontCard);
            const id = req.body._id;
            const priority = req.body.priority ? req.body.priority: "";
            const size = req.body.size ? req.body.size: "";
            const title = req.body.title ? req.body.title: "";
            const role = req.body.role ? req.body.role: "";
            const functionality = req.body.functionality ? req.body.functionality: "";
            const benefit = req.body.benefit ? req.body.benefit: "";

            let frontCard = new FrontCard({
                priority: priority,
                size: size,
                title: title,
                role: role,
                functionality: functionality,
                benefit: benefit
            });

            FrontCard.findOneAndUpdate({"_id": id}, frontCard, { new: true })
                .then(obj => res.status(200).json({
                    message: res.__('ok.replaceFrontCard'),
                    obj: obj
                }))
                .catch(ex => res.status(500).json({
                    message: res.__('bad.replaceFrontCard'),
                    obj: ex
                }));

        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    })
};

// PATCH /{id} update
function updateFrontCard(req, res, next) {
    req.ability.then(async (ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("UPDATE", FrontCard);
            const priority = req.body.priority;
            const size = req.body.size;
            const title = req.body.title;
            const role = req.body.role;
            const functionality = req.body.functionality;
            const benefit = req.body.benefit;
            
            let frontCard = new Object();

            if(priority) frontCard._priority = priority;
            if(size) frontCard._size = size;
            if(title) frontCard._title = title;
            if(role) frontCard._role = role;
            if(functionality) frontCard._functionality = functionality;
            if(benefit) frontCard._benefit = benefit;

            FrontCard.findOneAndUpdate({"_id": id}, FrontCard, { new: true })
                .then(obj => res.status(200).json({
                    message: res.__('ok.updateFrontCard'),
                    obj: obj
                }))
                .catch(ex => res.status(500).json({
                    message: res.__('bad.updateFrontCard'),
                    obj: ex
                }));

        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    })
};


// DELETE /{id} destroy
function destroyFrontCard(req, res, next) {
    req.ability.then((ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("DELETE", FrontCard);
            const id = req.params.id;
            FrontCard.remove({"_id": id})
                .then(obj => res.status(200).json({
                    message: res.__('ok.destroyFrontCard'),
                    obj: obj
                }))
                .catch(ex => res.status(500).json({
                    message: res.__('bad.destroyFrontCard'),
                    obj: ex
                }));
        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    })
};


module.exports = {
    getFrontCards,
    getFrontCard,
    createFrontCard,
    replaceFrontCard,
    updateFrontCard,
    destroyFrontCard
};
