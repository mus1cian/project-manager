const TeamMember = require("../models/teamMember");
const express = require('express');
const Record = require('../models/record');
const bcrypt = require("bcrypt");
const Profile = require("../models/profile");
const { default: mongoose } = require("mongoose");
const { ForbiddenError } = require("@casl/ability");

// method url action

// GET  list
function getMembers(req, res, next) {
    // req.ability.then((ability) => {
    //     try {
    //         ForbiddenError.from(ability).throwUnlessCan("READ", TeamMember);
    //         TeamMember.find().populate("_teamMembers")
    //             .then(objs => res.status(200).json({
    //                 message: res.__('ok.getTeamMembers'),
    //                 obj: objs
    //             }))
    //             .catch(ex => res.status(500).json({
    //                 message: res.__('bad.getTeamMembers'),
    //                 obj: ex
    //         }));
    //     } catch (error) {
    //         if (error instanceof ForbiddenError) {
    //             res.status(500).json({message: res.__('bad.login'), obj: error.message});
    //         }
    //     }
    // })
    TeamMember.find().populate("_teamMembers")
                .then(objs => res.status(200).json({
                    message: res.__('ok.getTeamMembers'),
                    obj: objs
                }))
                .catch(ex => res.status(500).json({
                    message: res.__('bad.getTeamMembers'),
                    obj: ex
            }));
};

// GET /{id} index
function getMember(req, res, next) {
    req.ability.then((ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("READ", TeamMember);
            const id = req.params.id;
            TeamMember.findOne({"_id": id})
            .then(obj => res.status(200).json({
                message: res.__('ok.getTeamMember'),
                obj: obj
            }))
            .catch(ex => res.status(500).json({
                message: res.__('bad.getTeamMember'),
                obj: ex
            }));
        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    });
};

// POST create
function createMember(req, res, next) {
    req.ability.then(async (ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("CREATE", TeamMember);
            const email = req.body.email;
            const password = req.body.passoword;
            const salt = req.body.salt;
            const name = req.body.name;
            const birthdate = req.body.birthdate;
            const curp = req.body.curp;
            const rfc = req.body.rfc;
            const skills = [];

            const profileId = req.body.profileId;
            const addressId = req.body.addressId;
            const recordId = req.body.recordId;

            let profile = await Profile.find({"_id": profileId});
            let record = await Record.find({"_id": recordId});
            let address = await Address.find({"_id": addressId});

            let teamMember = new TeamMember({
                _email: email,
                _password: password,
                _salt: salt,
                _profile: profile,
                _name: name,
                _birthdate: birthdate,
                _curp: curp,
                _rfc: rfc,
                _address: address,
                _skills: skills,
                _record: record
            });

            teamMember
                .save()
                .then((obj) =>
                res.status(200).json({
                    message: res.__('ok.createTeamMember'),
                    obj: obj,
                })
                )
                .catch((ex) =>
                res.status(500).json({
                    message: res.__('bad.createTeamMember'),
                    obj: ex,
                })
                );
        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    });
};


// PUT /{id} replace
function replaceMember(req, res, next) {
    req.ability.then(async (ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("UPDATE", TeamMember);
            const id = id;
            const email = req.body.email ? req.body.email: "";
            const password = req.body.password ? req.body.password: "";
            const salt = req.body.salt ? req.body.salt: 0;
            const name = req.body.name ? req.body.name: "";
            const birthdate = req.body.birthdate ? req.body.birthdate: "";
            const curp = req.body.curp ? req.body.curp: "";
            const rfc = req.body.rfc ? req.body.rfc: "";
            const skills = req.body.skills ? req.body.skills: [];

            const profileId = req.body.profileId;
            const addressId = req.body.addressId;
            const recordId = req.body.recordId;

            let profile = await Profile.find({"_id": profileId});
            let record = await record.find({"_id": recordId});
            let address = await Address.find({"_id": addressId});

            let teamMember = new TeamMember({
                _email: email,
                _password: password,
                _salt: salt,
                _profile: profile,
                _name: name,
                _birthdate: birthdate,
                _curp: curp,
                _rfc: rfc,
                _address: address,
                _skills: skills,
                _record: record
            });

            teamMember
                .save()
                .then((obj) =>
                res.status(200).json({
                    message: res.__('ok.replaceTeamMember'),
                    obj: obj,
                })
                )
                .catch((ex) =>
                res.status(500).json({
                    message: res.__('bad.replaceTeamMember'),
                    obj: ex,
                })
                );
        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    });
};

// PATCH /{id} update
function updateMember(req, res, next) {
    req.ability.then(async (ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("UPDATE", TeamMember);
            const email = req.body.email;
            const password = req.body.passoword;
            const salt = req.body.salt;
            const name = req.body.name;
            const birthdate = req.body.birthdate;
            const curp = req.body.curp;
            const rfc = req.body.rfc;
            const skills = [];

            const profileId = req.body.profileId;
            const addressId = req.body.addressId;
            const recordId = req.body.recordId;

            let profile = await Profile.find({"_id": profileId});
            let record = await record.find({"_id": recordId});
            let address = await Address.find({"_id": addressId});

            let teamMember = new TeamMember({
                _email: email,
                _password: password,
                _salt: salt,
                _profile: profile,
                _name: name,
                _birthdate: birthdate,
                _curp: curp,
                _rfc: rfc,
                _address: address,
                _skills: skills
            });

            teamMember
                .save()
                .then((obj) =>
                res.status(200).json({
                    message: res.__('ok.replaceTeamMember'),
                    obj: obj,
                })
                )
                .catch((ex) =>
                res.status(500).json({
                    message: res.__('bad.replaceTeamMember'),
                    obj: ex,
                })
                );
        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    });
};


// DELETE /{id} destroy
function destroyMember(req, res, next) {
    req.ability.then((ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("DELETE", TeamMember);
            const id = req.params.id;
            TeamMember.remove({"_id": id})
                .then(obj => res.status(200).json({
                    message: res.__('ok.destroyTeamMember'),
                    obj: obj
                }))
                .catch(ex => res.status(500).json({
                    message: res.__('bad.destroyTeamMember'),
                    obj: ex
                }));
        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    });
};


module.exports = {
    getMembers,
    getMember,
    createMember,
    replaceMember,
    updateMember,
    destroyMember
};

