const express = require('express');
const { configure } = require('../rbac');

// method url action

// GET  list
function getBurnDownCharts(req, res, next) {
    req.ability.then((ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("READ", BurnDownChart);
            BurnDownChart.find().populate("_burnDownCharts")
                .then(objs => res.status(200).json({
                    message: res.__('ok.getBurnDownCharts'),
                    obj: objs
                }))
                .catch(ex => res.status(500).json({
                    message: res.__('bad.getBurnDownCharts'),
                    obj: ex
            }));
        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    })
};

// GET /{id} index
function getBurnDownChart(req, res, next) {
    req.ability.then((ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("READ", BurnDownChart);
            const id = req.params.id;
            BurnDownChart.findOne({"_id": id})
            .then(obj => res.status(200).json({
                message: res.__('ok.getBurnDownChart'),
                obj: obj
            }))
            .catch(ex => res.status(500).json({
                message: res.__('bad.getBurnDownChart'),
                obj: ex
            }));
        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    })
};

// POST create
function createBurnDownChart(req, res, next) {
    req.ability.then(async (ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("CREATE", BurnDownChart);
            const sprints = req.body.sprints;
            const storyPoints = req.body.sprints;
            const recordId = req.body.recordId;
            
            let record = await Record.find({"_id": recordId});
            
            let burnDownChart = new BurnDownChart({
                sprints: sprints,
                storyPoints: storyPoints,
                record: record
            });

            burnDownChart
                .save()
                .then((obj) =>
                res.status(200).json({
                    message: res.__('ok.createBurnDownChart'),
                    obj: obj,
                })
                )
                .catch((ex) =>
                res.status(500).json({
                    message: res.__('bad.createBurnDownChart'),
                    obj: ex,
                })
                );

        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    })
};


// PUT /{id} replace
function replaceBurnDownChart(req, res, next) {
    req.ability.then(async (ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("UPDATE", BurnDownChart);
            const id = req.params.id;
            const sprints = req.body.sprints ? req.body.sprints: 0;
            const storyPoints = req.body.sprints ? req.body.storyPoints: 0;
            const recordId = req.body.recordId ? req.body.recordId: "";

            let record = await Record.find({"_id": recordId});

            let burnDownChart = new BurnDownChart({
                sprints: sprints,
                storyPoints: storyPoints,
                record: record
            });

            BurnDownChart.findOneAndUpdate({"_id": id}, burnDownChart, { new: true })
                .then(obj => res.status(200).json({
                    message: res.__('ok.replaceBurnDownChart'),
                    obj: obj
                }))
                .catch(ex => res.status(500).json({
                    message: res.__('bad.replaceBurnDownChart'),
                    obj: ex
                }));

        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    })
};

// PATCH /{id} update
function updateBurnDownChart(req, res, next) {
    req.ability.then(async (ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("UPDATE", BurnDownChart);
            const id = req.params.id;
            const sprints = req.body.sprints;
            const storyPoints = req.body.sprints;
            const recordId = req.body.recordId;
            
            let record = await Record.find({"_id": recordId});
            
            let burnDownChart = new Object();

            if(sprints) burnDownChart._sprints = sprints;
            if(storyPoints) burnDownChart._storyPoints = storyPoints;
            if(record) burnDownChart._record = record;

            BurnDownChart.findOneAndUpdate({"_id": id}, burnDownChart, { new: true })
                .then(obj => res.status(200).json({
                    message: res.__('ok.updateBurnDownChart'),
                    obj: obj
                }))
                .catch(ex => res.status(500).json({
                    message: res.__('bad.updateBurnDownChart'),
                    obj: ex
                }));

        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    })
};


// DELETE /{id} destroy
function destroyBurnDownChart(req, res, next) {
    req.ability.then((ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("DELETE", BurnDownChart);
            const id = req.params.id;
            BurnDownChart.remove({"_id": id})
                .then(obj => res.status(200).json({
                    message: res.__('ok.destroyBurnDownChart'),
                    obj: obj
                }))
                .catch(ex => res.status(500).json({
                    message: res.__('bad.destroyBurnDownChart'),
                    obj: ex
                }));
        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    })
};


module.exports = {
    getBurnDownCharts,
    getBurnDownChart,
    createBurnDownChart,
    replaceBurnDownChart,
    updateBurnDownChart,
    destroyBurnDownChart
};
