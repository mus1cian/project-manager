const express = require('express');
const { configure } = require('../rbac');

// method url action

// GET  list
function getRecordCards(req, res, next) {
    req.ability.then((ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("READ", RecordCard);
            RecordCard.find().populate("_recordCards")
            .then(objs => res.status(200).json({
                message: res.__('ok.getRecordCards'),
                obj: objs
            }))
            .catch(ex => res.status(500).json({
                message: res.__('bad.getRecordCards'),
                obj: ex
        }));
        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    })

};

// GET /{id} index
function getRecordCard(req, res, next) {
    req.ability.then((ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("READ", RecordCard);
            const id = req.params.id;
            RecordCard.findOne({"_id": id})
            .then(obj => res.status(200).json({
                message: res.__('ok.getRecordCard'),
                obj: obj
            }))
            .catch(ex => res.status(500).json({
                message: res.__('bad.getRecordCard'),
                obj: ex
            }));
        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    })
    // req.params nos permite acceder a los parametros de la url
    // req.body nos permite acceder a los parametros de la peticion
    

};

// POST create
function createRecordCard(req, res, next) {
    req.ability.then(async (ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("CREATE", RecordCard);
            const recordId = req.body.record;
            const frontCard = req.body.frontCard;
            const backCard = req.body.backCard;
            const value = req.body.value;
            const column = req.body.column;

            let record = await Record.find({"_id": recordId});

            let recordCard = new Profile({
                record: record,
                frontCard: frontCard,
                backCard: backCard,
                value: value,
                column: column
            });

            recordCard
                .save()
                .then((obj) =>
                res.status(200).json({
                    message: res.__('ok.createRecordCard'),
                    obj: obj,
                })
                )
                .catch((ex) =>
                res.status(500).json({
                    message: res.__('bad.createRecordCard'),
                    obj: ex,
                })
                );
        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    })
    
};


// PUT /{id} replace
function replaceRecordCard(req, res, next) {
    req.ability.then(async (ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("UPDATE", RecordCard);
            const recordId = req.body.record ? req.body.record: "";
            const frontCard = req.body.frontCard ? req.body.front: "";
            const backCard = req.body.backCard ? req.body.back : "";
            const value = req.body.value ? req.body.value : "";;
            const column = req.body.column ? req.body.column: "";

            let record = await Record.find({"_id": recordId});

            let recordCard = new Profile({
                record: record,
                frontCard: frontCard,
                backCard: backCard,
                value: value,
                column: column
            });

            Profile.findOneAndUpdate({"_id": id}, recordCard, { new: true })
            .then(obj => res.status(200).json({
                message: res.__('ok.replaceRecordCard'),
                obj: obj
            }))
            .catch(ex => res.status(500).json({
                message: res.__('bad.replaceRecordCard'),
                obj: ex
            }));

        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    })
    
};

// PATCH /{id} update
function updateRecordCard(req, res, next) {
    req.ability.then(async (ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("UPDATE", RecordCard);
            const recordId = req.body.record;
            const frontCard = req.body.frontCard;
            const backCard = req.body.backCard;
            const value = req.body.value;
            const column = req.body.column;

            let record = await Record.find({"_id": recordId});

            let recordCard = new Object();

            if (recordId) recordCard._record = record;
            if (frontCard) recordCard._frontCard = frontCard;
            if (backCard) recordCard._backCard = backCard;
            if (value) recordCard._value = value;
            if (column) recordCard._column = column;            

            RecordCard.findOneAndUpdate({"_id": id}, recordCard, { new: true })
            .then(obj => res.status(200).json({
                message: res.__('ok.updateRecordCard'),
                obj: obj
            }))
            .catch(ex => res.status(500).json({
                message: res.__('bad.updateRecordCard'),
                obj: ex
            }));

        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    })
    
};


// DELETE /{id} destroy
function destroyRecordCard(req, res, next) {
    req.ability.then((ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("DELETE", RecordCard);
            const id = req.params.id;
            RecordCard.remove({"_id": id})
                .then(obj => res.status(200).json({
                    message: res.__('ok.updateRecordCard'),
                    obj: obj
                }))
                .catch(ex => res.status(500).json({
                    message: res.__('bad.updateRecordCard'),
                    obj: ex
                }));
        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    })
    
};


module.exports = {
    getRecordCards,
    getRecordCard,
    createRecordCard,
    replaceRecordCard,
    updateRecordCard,
    destroyRecordCard
};
