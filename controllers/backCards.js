const express = require('express');
const { configure } = require('../rbac');

// method url action

// GET  list
function getBackCards(req, res, next) {
    req.ability.then((ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("READ", BackCard);
            BackCard.find().populate("_backCard")
                .then(objs => res.status(200).json({
                    message: res.__('ok.getBackCards'),
                    obj: objs
                }))
                .catch(ex => res.status(500).json({
                    message: res.__('bad.getBackCards'),
                    obj: ex
            }));
        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    })
};

// GET /{id} index
function getBackCard(req, res, next) {
    req.ability.then((ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("READ", BackCard);
            const id = req.params.id;
            BackCard.findOne({"_id": id})
            .then(obj => res.status(200).json({
                message: res.__('ok.getBackCard'),
                obj: obj
            }))
            .catch(ex => res.status(500).json({
                message: res.__('bad.getBackCard'),
                obj: ex
            }));
        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    })
};

// POST create
function createBackCard(req, res, next) {
    req.ability.then((ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("CREATE", BackCard);
            const criteria = req.body.criteria;
            const context = req.body.context;
            const events = [];
            const results = [];

            let backCard = new BackCard({
                criteria: criteria,
                context: context,
                events: events,
                results: results
            });

            backCard
                .save()
                .then((obj) =>
                res.status(200).json({
                    message: res.__('ok.createBackCard'),
                    obj: obj,
                })
                )
                .catch((ex) =>
                res.status(500).json({
                    message: res.__('bad.createBackCard'),
                    obj: ex,
                })
                );

        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    })
};


// PUT /{id} replace
function replaceBackCard(req, res, next) {
    req.ability.then(async (ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("UPDATE", BackCard);
            const id = req.params.id;
            const criteria = req.body.criteria ? req.body.criteria: "";
            const context = req.body.context ? req.body.context: "";
            const events = req.body.events ? req.body.events: [];
            const results = req.body.results ? req.body.results: [];

            let backCard = new Object({
                _id: id,
                _criteria: criteria,
                _context: context,
                _events: events,
                _results: results
            });

            BackCard.findOneAndUpdate({"_id": id}, backCard, { new: true })
                .then(obj => res.status(200).json({
                    message: res.__('ok.replaceBackCard'),
                    obj: obj
                }))
                .catch(ex => res.status(500).json({
                    message: res.__('bad.replaceBackCard'),
                    obj: ex
                }));

        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    })
};

// PATCH /{id} update
function updateBackCard(req, res, next) {
    req.ability.then(async (ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("UPDATE", BackCard);
            const criteria = req.body.criteria;
            const context = req.body.context;
            const events = req.body.events;
            const results = req.body.results;
            
            let backCard = new Object();

            if(criteria) backCard._criteria = criteria;
            if(context) backCard._context = context;
            if(events) backCard._events = events;
            if(results) backCard._results = results;

            BackCard.findOneAndUpdate({"_id": id}, backCard, { new: true })
                .then(obj => res.status(200).json({
                    message: res.__('ok.updateBackCard'),
                    obj: obj
                }))
                .catch(ex => res.status(500).json({
                    message: res.__('bad.updateBackCard'),
                    obj: ex
                }));

        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    })
};


// DELETE /{id} destroy
function destroyBackCardCard(req, res, next) {
    req.ability.then((ability) => {
        try {
            ForbiddenError.from(ability).throwUnlessCan("DELETE", BackCard);
            const id = req.params.id;
            BackCard.remove({"_id": id})
                .then(obj => res.status(200).json({
                    message: res.__('ok.destroyBackCard'),
                    obj: obj
                }))
                .catch(ex => res.status(500).json({
                    message: res.__('bad.destroyBackCard'),
                    obj: ex
                }));
        } catch (error) {
            if (error instanceof ForbiddenError) {
                res.status(500).json({message: res.__('bad.login'), obj: error.message});
            }
        }
    })
};


module.exports = {
    getBackCards,
    getBackCard,
    createBackCard,
    replaceBackCard,
    updateBackCard,
    destroyBackCardCard
};

