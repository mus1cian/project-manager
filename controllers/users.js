const express = require("express");
const bcrypt = require("bcrypt");
const User = require("../models/user");
const Profile = require("../models/profile");
const { default: mongoose } = require("mongoose");
const { ForbiddenError } = require("@casl/ability");

async function list(req, res, next) {
  req.ability.then((ability) => {
    try {
        ForbiddenError.from(ability).throwUnlessCan("READ", Profile);
        User.find().populate("_profiles")
            .then(objs => res.status(200).json({
                message: res.__('find.profiles'),
                obj: objs
            }))
            .catch(ex => res.status(500).json({
                message: "No se pudo consultar la lista de usuarios",
                obj: ex
        }));
    } catch (error) {
        if (error instanceof ForbiddenError) {
            res.status(500).json({message: res.__('bad.login'), obj: error.message});
        }
    }
})
}

function index(req, res, next) {
  req.ability.then((ability) => {
    try {
      ForbiddenError.from(ability).throwUnlessCan("READ", User);
      const id = req.params.id;
      User.findOne({ _id: id })
        .then((objs) =>
          res.status(200).json({
            message: `USERS FIND ${id}`,
            obj: objs,
          })
        )
        .catch((ex) =>
          res.status(500).json({
            message: `USERS WITH ID ${id} NOT FOUND`,
            obj: ex,
          })
        );
    } catch (error) {
      if (error instanceof ForbiddenError) {
        res.status(500).json({
          message: "ERROR WITH PERMISSIONS",
          obj: error.message,
        });
      }
    }
  });
}

async function create(req, res, next) {
  const name = req.body.name;
  const lastName = req.body.lastName;
  const email = req.body.email;
  const password = req.body.password;

  const salt = await bcrypt.genSalt(10);
  const passwordHash = await bcrypt.hash(password, salt);

  let user = new User({
    name: name,
    lastName: lastName,
    email: email,
    password: passwordHash,
    salt: salt,
  });

  if (
    name == undefined ||
    lastName == undefined ||
    email == undefined ||
    password == undefined
  )
    res.status(403).json({
      message: "USER CREATE ERROR",
      obj: "ERROR",
    });

  req.ability.then((ability) => {
    try {
      ForbiddenError.from(ability).throwUnlessCan("CREATE", User);
      user
        .save()
        .then((obj) =>
          res.status(200).json({
            message: "USER CREATE SUCCESS",
            obj: obj,
          })
        )
        .catch((ex) =>
          res.status(500).json({
            message: "USER CREATE ERROR",
            obj: ex,
          })
        );
    } catch (error) {
      if (error instanceof ForbiddenError) {
        res.status(500).json({
          message: "ERROR WITH PERMISSIONS",
          obj: error.message,
        });
      }
    }
  });
}

async function replace(req, res, next) {
  req.ability.then(async (ability) => {
    try {
      ForbiddenError.from(ability).throwUnlessCan("UPDATE", User);
      const id = req.params.id;
      const name = req.body.name ? req.body.name : "";
      const lastName = req.body.lastName ? req.body.lastName : "";
      const email = req.body.email ? req.body.email : "";
      const password = req.body.password ? req.body.password : "";

      const salt = await bcrypt.genSalt(10);
      const passwordHashGen = await bcrypt.hash(password, salt);

      let user = new Object({
        _name: name,
        _lastName: lastName,
        _email: email,
        _password: passwordHashGen,
        _salt: salt,
      });

      User.findOneAndUpdate({ _id: id }, user, { new: true })
        .then((obj) =>
          res.status(200).json({
            message: `Replace USER with id: ${id}`,
            obj: obj,
          })
        )
        .catch((ex) =>
          res.status(500).json({
            message: `Replace USER with id: ${id} ERROR`,
            obj: ex,
          })
        );
    } catch (error) {
      if (error instanceof ForbiddenError) {
        res.status(500).json({
          message: "ERROR WITH PERMISSIONS",
          obj: error.message,
        });
      }
    }
  });
}

function update(req, res, next) {
  req.ability.then(async (ability) => {
    try {
      ForbiddenError.from(ability).throwUnlessCan("UPDATE", User);
      const id = req.params.id;
      const name = req.body.name;
      const lastName = req.body.lastName;
      const email = req.body.email;
      const password = req.body.password;
      const salt = await bcrypt.genSalt(10);

      const passwordHashGen = await bcrypt.hash(password, salt);

      let user = new Object();

      if (name) user._name = name;
      if (lastName) user._lastName = lastName;
      if (email) user._email = email;
      if (password) user._password = passwordHashGen;
      if (salt) user._salt = salt;

      User.findOneAndUpdate({ _id: id }, user, { new: true })
        .then((obj) =>
          res.status(200).json({
            message: `UPDATE object with id: ${id}`,
            obj: obj,
          })
        )
        .catch((ex) =>
          res.status(500).json({
            message: `UPDATE object with id: ${id} ERROR`,
            obj: ex,
          })
        );
    } catch (error) {
      if (error instanceof ForbiddenError) {
        res.status(500).json({
          message: "ERROR WITH PERMISSIONS",
          obj: error.message,
        });
      }
    }
  });
}

function destroy(req, res, next) {
  req.ability.then(async (ability) => {
    try {
      ForbiddenError.from(ability).throwUnlessCan("DELETE", User);
      const id = req.params.id;
      User.remove({ _id: id })
        .then((obj) =>
          res.status(200).json({
            message: `DELETE USER with id: ${id}`,
            obj: obj,
          })
        )
        .catch((ex) =>
          res.status(500).json({
            message: `DELETE USER with id: ${id} ERROR`,
            obj: ex,
          })
        );
    } catch (error) {
      if (error instanceof ForbiddenError) {
        res.status(500).json({
          message: "ERROR WITH PERMISSIONS",
          obj: error.message,
        });
      }
    }
  });
}

module.exports = {
  list,
  index,
  create,
  replace,
  update,
  destroy
};
