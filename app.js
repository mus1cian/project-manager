const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const { expressjwt: jwt } = require('express-jwt');
const i18n = require('i18n');
const mongoose = require('mongoose');
const config = require('config');
const auth = require('./auth')
const i18n = require("i18n");

const config = require('config');

const teamMembers = require('./routes/teamMembers');
const recordCardsRouter = require('./routes/recordCards');
const recordsRouter = require('./routes/records');
const teamMembersRouter = require('./routes/teamMembers');
const addressRouter = require('./router/address');
const backCardRouter = require('/router/backCard');
const frontCard = require('./routes/frontCard');
const permissionsRouter = require('./router/permissionsRouter');
const profilesRouter = require('./routes/profiles');
const burnDownChartsRouter = require('./route/burnDownChart');
const usersRouter = require('./route/users');

const uri = "mongodb+srv://brian:1234@cluster0.xdubogw.mongodb.net/?retryWrites=true&w=majority";

mongoose.connect(uri, {
          useNewUrlParser: true,
          useUnifiedTopology: true,
          dbName: 'project_manager',
        })
        .then(() => {
          console.log('Connected to the Database.');
        })
        .catch(err => console.error(err));

i18n.configure({
  locales: ['en', 'es'],
  cookie: 'language',
  directory: path.join(__dirname, 'locales')
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(i18n.init);

const app = express();

const indexRouter = require('./routes/index');
app.use('/',indexRouter);
app.use('/profiles', profilesRouter);
app.use('.recordCards', recordCardsRouter);
app.use('/users', usersRouter);
app.use('/permissions', permissionsRouter);
app.use('/teamMembers', teamMembers);
app.use('./address', addressRouter);
app.use('/records', recordsRouter);
app.use('/teamMembers', teamMembersRouter);
app.use('/burnDownChart', burnDownChartsRouter);
app.use('/backCard', backCardRouter);
app.use('/frontCard', frontCard);
app.use('/permissions', permissionsRouter);
app.use('/profiles', profilesRouter);
app.use('/users', usersRouter);

const jwtKey = config.get('secret.key');
app.use(jwt({secret: jwtKey, algorithms: ['HS256']})
  .unless({path: ['/login']}));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
