# Project Manager
## Descripción del proyecto

En este README se describe nuestra propuesta para un software de manejo de control de proyectos basado en la metodología ágil Scrum.

# Diagramas

## Diagrama de Clases
![diagrama de clases](./public/images/classDiagram.png)

### Descripción de la estructura
Nuestro proyecto consiste en las siguientes 3 clases principales:
* **Record**.- Que corresponde a los expedientes por proyecto.
* **TeamMember**.- Los cuales serán cada miembro del equipo desarrollador del expediente, adicionalmente esta clase cuenta con:
    1. Una sub-clase para poder desglosar de mejor forma la información del Address del miembro del equipo
    2. Dos Enums que corresponderán al lenguaje de programación que domina y el nivel que tiene en el mismo. Estas dos características se colocarán dentro de la lista de habilidades del miembro.
* **RecordCard**.- Una clase que se conecta con el expediente del proyecto. Corresponde a una tarjeta de historial de usuario. Se encuentra dividida en 2 sub-clases que representan en derecho y reverso de la tarjeta, las cuales cuentan con características individuales. A su vez, contiene un Enum para especificar la columna en la que la tarjeta se encuentra, así como el burndown chart asociado.

## Diagrama de Secuencia
![Image](./public/images/diagramaSecuencia.png)

El diagrama de secuencia de la aplicación Project Manager incluye las diferentes funciones que se permitirán realizar a través de los distintos componentes y actores. 

### Descripción de la estructura
Los componentes que contiene son:
* **Log in screen**: Pantalla de inicio de sesión con 3 diferentes redes sociales
* **UI**: Interfaz gráfica donde se muestra la información del tablero, miembros del equipo, proyecto, etc.
* **Controller**: Conexión entre vista (interfaz gráfica) y modelo (base de datos)
* **Model**: Se encarga de realizar cambios a la base de datos
* **DB**: Base de datos que contiene los datos sobre los distintos proyectos

Incluye las siguientes funcionalidades:

* Operaciones CRUD de los distintos proyectos y miembros de los equipos
* Conexión a base de datos no relacional
* Interfaz gráfica

# Docker images

## Heroku
Para manejar la aplicación, utilizamos Heroku, la cual es una Plataforma como Servicio (PaaS) basada en la nube.

[Link del proyecto en Heroku](https://dashboard.heroku.com/apps/project-manager-wp)

## Docker Hub
Como servicio de contenedor e imágenes, se utilizó Docker Hub.

[Link del proyecto en Docker Hub](https://hub.docker.com/repository/docker/valerianevarez/project-manager)


# Integrantes
* Valeria Sofia Nevárez Juárez
* Marley Zaragoza Balderrama
* Juan Luis de Valle Sotelo
* Brian Jair Acosta Loera