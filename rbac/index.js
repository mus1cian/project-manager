const permissions = require("./permissions");
 
module.exports = {
  configure(app) {
    app.use((req, res, next) => {
        console.log(req.auth)
      req.ability = permissions.setPermissionsForUser(req.auth.user);
      next();
    });
  },
};