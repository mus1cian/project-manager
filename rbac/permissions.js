const { AbilityBuilder, PureAbility } = require("@casl/ability");
const User = require("../models/user");
const mongoose = require("mongoose");

async function setPermissionsForUser(user) {
  const { can, rules } = new AbilityBuilder(PureAbility);
  let permission = await User.aggregate([
    {
        $match: {
          $expr: {
            $eq: ['$_id', { $toObjectId: user["_id"] }],
          },
        },
      },
    {
        $lookup: {
            from: 'profiles',
           localField: '_profiles',
           foreignField: '_id',
           as: '_profiles'
        }
    },
    {
        $lookup: {
           from: 'permissions',
           localField: '_profiles._permissions',
           foreignField: '_id',
           as: '_permissions'
        }
    },
  ]);

  if(permission && permission.length > 0) {
    let permissions = permission[0]._permissions;
    for (let i = 0; i < permissions.length; i++) {
        can(permissions[i]._type,mongoose.model(permissions[i]._description))
    }
  }

  return new PureAbility(rules);
}

module.exports = { setPermissionsForUser };

